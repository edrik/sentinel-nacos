@REM @Author: Amber
@REM @Date:   2021-08-11 15:03:33
@REM @Last Modified by:   Amber
@REM Modified time: 2021-08-11 15:07:40

start java -Dnacos.address=192.168.100.232:8848 -Dserver.port=18719 -Dcsp.sentinel.api.port=8719 -Dcsp.sentinel.dashboard.server=localhost:18719 -Dproject.name=sentinel-dashboard -jar ./target/sentinel-dashboard-nacos-1.8.2.jar
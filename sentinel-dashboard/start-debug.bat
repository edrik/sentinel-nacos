@REM @Author: Amber
@REM @Date:   2021-08-11 14:59:41
@REM @Last Modified by:   Amber
@REM Modified time: 2021-08-11 15:05:20


mvnw spring-boot:run -Dspring-boot.run.arguments=--nacos.address=192.168.100.232:8848,--server.port=18719,--csp.sentinel.dashboard.server=localhost:18719


